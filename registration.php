<!DOCTYPE html>
<html>
    <head>
        <title>Sign Up to Netflix</title>
        <link rel="stylesheet" href="css/signout.css">
        <link rel="stylesheet" href="Plugins/font-awesome-4.7.0/css/font-awesome.css">
        <link rel="stylesheet" media="screen and (max-width: 768px)"
    href="css/mobile.css">
    </head>
    <style>
    	body{
    		font-family: Helvetica,Arial,sans-serif;
/*    		font-size: 0.7rem;*/
        }
        a{
            /* padding:30px; */
        }
        input:invalid {
  border-style: 1px red solid;
}

input:valid {
  background-color: #ddffdd;
}

input:required {
  border-color: #800000;
  border-width: 3px;
}
    </style>
    <body>
        <header>
            <a href="index.php"><img src="images/netflixfinal.png" class="logo" alt="netflix-logo" width="auto" height="50px" style="margin: 17px 20px 20px 10px;"></a>
            <a href="sign-in.html" class="sign-in pull-right mobs">Sign In</a>
            <div class="clearfix"></div>
        </header>
<?php
	require('db.php');
    // If form submitted, insert values into the database.
    if (isset($_REQUEST['first_name'])){
//		$first_name = stripslashes($_REQUEST['first_name']); // removes backslashes
//		$first_name = mysqli_real_escape_string($con,$first_name); //escapes special characters in a string
//        $last_name = stripslashes($_REQUEST['last_name']); // removes backslashes
//		$last_name = mysqli_real_escape_string($con,$last_name);
//        $contact = stripslashes($_REQUEST['contact']); // removes backslashes
//		$contact = mysqli_real_escape_string($con,$contact); //escapes special characters in a string
//        $address = stripslashes($_REQUEST['address']); // removes backslashes
//		$address = mysqli_real_escape_string($con,$address);
		$email = stripslashes($_REQUEST['email']);
		$email = mysqli_real_escape_string($con,$email);
		$password = stripslashes($_REQUEST['password']);
		$password = mysqli_real_escape_string($con,$password);
//		$trn_date = date("Y-m-d H:i:s");
        $query = "INSERT into `guest` (first_name,last_name,contact,address,email,password) VALUES ('','','','','$email','$password')";
        $result = mysqli_query($con,$query);
        if($result){
            header("Location: payment.php");
            echo "<div class='form'><h3>You are registered successfully.</h3><br/>Click here to <a href='login.php'>Login</a></div>";
        }
    }else{
?>
<div class="sign-up-div">

<p class="step">STEP <strong>2</strong> OF <strong>3</strong></p>
           <p class="sign"><strong>Sign up to start your free month</strong></p>
           <h4>Create your account</h4>
<form name="registration" class="form" action="" method="post">
    <div class="sign-up-div">
       <div class="row">
            <div class="input-field col s12">
        <input type="text" name="first_name" placeholder="firstname" hidden />
           </div></div>
           <div class="row">
            <div class="input-field col s12">
        <input type="text" name="last_name" placeholder="lastname" hidden />
           </div></div>
           <div class="row">
            <div class="input-field col s12">
                <input id="contact" type="text" name="contact" placeholder="Contact" hidden>
                          
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <input id="address" type="text" name="address" placeholder="Address" hidden>
                          
            </div>
        </div>
           <input type="email" class="email" placeholder="Email" name="email" required>
           <input type="password" class="password" placeholder="Password" name="password" minlength="8" required>
           
            <a href="payment.php"><button class="sign-button">SIGN UP</button></a>
            
        </div>
    </form>
       <footer id="footer">
        <!--            <p>Questions? Contact Us</p>-->
                    <ul>
                       <li><a href="">Terms and Private Policy</a></li>
                        <li><a href="">FAQ</a></li>
                        <li><a href="">Help Centre</a></li>
                        <li><a href="">Send Us Feedback</a></li>
                    </ul>
                    <p>Copyright &copy; 2019 <span>Netflix</span> , All Rights Reserved.</p>
        </footer>
        <?php } ?>
        </div>
    </body>
</html>