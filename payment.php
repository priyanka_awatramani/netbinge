
<html onload="change()">
    <head>
        <title>Payment</title>
        <link rel="stylesheet" href="css/payment.css">
        <link rel="stylesheet" href="Plugins/font-awesome-4.7.0/css/font-awesome.css">
        <link rel="stylesheet" media="screen and (max-width: 768px)"
    href="css/mobile.css">
    </head>
    <style>
    	body{
    		font-family: Helvetica,Arial,sans-serif;
/*    		font-size: 0.7rem;*/
        }
        
    </style>
    <body >
        <header>
            <a href="index.php"><img src="images/netflixfinal.png" class="logo" alt="netflix-logo" width="auto" height="50px" style="margin: 17px 20px 20px 10px;"></a>
            <a href="login.php" class="sign-in pull-right">Sign In</a>
            <div class="clearfix"></div>
        </header>
        <form action="http://localhost/netflix/conn.php" method="POST"></form>
       <div class="sign-up-div" id="mob">
           <p class="step">STEP <strong>3</strong> OF <strong>3</strong></p>
           <p class="sign"><strong>Set up your credit or debit card.</strong></p>
           <ul class="inline">
               <li><img src="images/visa.svg" alt="" width="auto" height="40px"></li>
               <li><img src="images/mastercard.svg" alt="" width="auto" height="40px"></li>
               <li><img src="images/amex.svg" alt="" width="auto" height="40px"></li>
               <li><img src="images/diners-club.svg" alt="" width="auto" height="40px"></li>
           </ul>
           <form name="" class="form" action="" onSubmit="alert()" method="post"> 
           <input type="text" class="email" placeholder="First Name" name="email" required>
           <input type="text"class="email" placeholder="Last Name" name="password" required>
           <input type="text" class="email" placeholder="Card Number" name="email" required>
           <input type="text" class="email" placeholder="Expiry Date (MM/YY)" name="email" required>
           <input type="text" class="email" placeholder="Security Code (CVV)" name="email" required>
           
           <div class="grey"><strong><p id="choice">Premium Plan</p></strong><p id="" class="in"><i class="fa fa-inr"></i> <span id="choice1"> 799 / mo. after free trial</span></p><span class="badge"><a href="plans.php" class="change">Change</a></span></div>
           <div class="clearfix"></div>
           <p class="inst">By ticking the tickbox below, you agree to our <a href="">Terms of Use</a>, <a href="">Privacy Statement</a>, and that you are over 18. You may cancel at any time during your free trial and will not be charged. Netbinge will automatically continue your membership at the end of your free trial and charge the membership fee to your payment method on a monthly basis until you cancel.</p>
           <input type="checkbox" class="checkbox" required><p class="agree"> I agree.</p>
           <a href="home.php"><button class="sign-button">START MEMBERSHIP</button></a>
           </form>
       </div>
       <footer id="footer">
        <!--            <p>Questions? Contact Us</p>-->
                    <ul>
                       <li><a href="">Terms and Private Policy</a></li>
                        <li><a href="">FAQ</a></li>
                        <li><a href="">Help Centre</a></li>
                        <li><a href="">Send Us Feedback</a></li>
                    </ul>
                    <p>Copyright &copy; 2019 <span>Netbinge</span> , All Rights Reserved.</p>
        </footer>
        <script>
            function alert(){
                confirm("Your payment was successful! Sign In to continue to Netbinge!");
            }
        </script>
        <script>
            var text = localStorage['plan'];
            localStorage.removeItem( 'plan' );
            console.log(text);
            if(text=='Mobile'){
        console.log('t');
        document.getElementById("choice").innerHTML='Mobile Plan';
        document.getElementById("choice1").innerHTML='199 / mo. after free trial';
    }
    if(text=='Basic'){
        console.log('t1');
        document.getElementById("choice").innerHTML='Basic Plan';
        document.getElementById("choice1").innerHTML='499 / mo. after free trial';
    }
    if(text=='Standard'){
        console.log('t2');
        document.getElementById("choice").innerHTML='Standard Plan';
        document.getElementById("choice1").innerHTML='649 / mo. after free trial';
    }
    if(text=='Premium'){
        console.log('t3');
        document.getElementById("choice").innerHTML='Premium Plan';
        document.getElementById("choice1").innerHTML='799 / mo. after free trial';
    }
        </script>
    </body>
</html>