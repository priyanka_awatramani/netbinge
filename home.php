<html>

<head>
    <title>Home Page</title>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/g/jquery.owlcarousel@1.31(owl.carousel.css+owl.theme.css)">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/fontawesome/4.6.3/css/font-awesome.min.css">
    <link href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/owl.carousel.js"></script>


    <!--<script src="https://cdn.jsdelivr.net/jquery/2.2.4/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/jquery.owlcarousel/1.31/owl.carousel.min.js"></script>
  <style>-->

    <!--This tag is both JS files combined-->
    <script src="https://cdn.jsdelivr.net/g/jquery@2.2.4,jquery.owlcarousel@1.31"></script>
    
</head>
<style>
        .item {
            width: 100%;
        }

        #owl-example {
            width: 100%;
        }

        #owl-example .item {

            margin-left: 20px;
            margin-right: 20px;


        }

        .owl-item:nth-child(3n+1)>.item {
            margin-left: 0;
        }

        .owl-item:nth-child(3n+3)>.item {
            margin-right: 0;
        }

        .owl-carousel .owl-item {
            margin-right: 20px;
        }

    </style>


<body>
    <header class="main-page-header sticky">
        <a href="index.php"><img src="images/netflixfinal.png" class="logo" alt="netflix-logo" width="auto" height="50px" style="margin: 17px 20px 20px 10px;"></a>
    </header>
    <div class="main-container">
        <div class="row1 row">
            <h4>Comic Book &amp; Superhero TV</h4>
            <div id="qunit"></div>
            <div id="qunit-fixture">
                <div id="owl-example" class="owl-carousel owl-theme">
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r11.jpg" alt="">
                                <p>Marvel's Iron Fist</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="protector.php">
                            <div class="block pull-left">
                                <img src="images/r12.jpg" alt="">
                                <p>The Protector</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r13.jpg" alt="">
                                <p>Marvel's Daredevil</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r14.jpg" alt="">
                                <p>Marvel's Jessica Jones</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r15.jpg" alt="">
                                <p>The Umbrella Academy</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r16.jpg" alt="">
                                <p>Titans</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r17.jpg" alt="">
                                <p>Marvel's Luke Cage</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r18.jpg" alt="">
                                <p>Ultraman</p>
                            </div>
                        </a></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="row2 row">
            <h4>Teen Programmes</h4>
            <div id="qunit"></div>
            <div id="qunit-fixture">
                <div id="owl-example" class="owl-carousel owl-theme">
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r21.jpg" alt="">
                                <p>The Society</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r22.jpg" alt="">
                                <p>Chilling Adventures of Sabrina</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r23.jpg" alt="">
                                <p>The End of the F***ing World</p>
                            </div>
                        </a></div>
                    <div class="item">

                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r24.jpg" alt="">
                                <p>Shadowhunters: The Mortal Instruments</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r25.jpg" alt="">
                                <p>The Rain</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r26.jpg" alt="">
                                <p>Trinkets</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r27.jpg" alt="">
                                <p>Insatiable</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r28.jpg" alt="">
                                <p>Elite</p>
                            </div>
                        </a></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="row3 row">
            <h4>TV Thrillers</h4>
            <div id="qunit"></div>
            <div id="qunit-fixture">
                <div id="owl-example" class="owl-carousel owl-theme">
                    <div class="item">
                        <a href="sacred-games.php">
                            <div class="block pull-left">
                                <img src="images/r31.jpg" alt="">
                                <p>Sacred Games</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r32.jpg" alt="">
                                <p>Stranger Things</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r33.jpg" alt="">
                                <p>Narcos</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r34.jpg" alt="">
                                <p>You</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r35.jpg" alt="">
                                <p>Black Mirror</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r36.jpg" alt="">
                                <p>Ghoul</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r37.jpg" alt="">
                                <p>House of Cards</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r38.jpg" alt="">
                                <p>Mindhunter</p>
                            </div>
                        </a></div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="row4 row">
            <h4>Action &amp; Adventure Programmes</h4>
            <div id="qunit"></div>
            <div id="qunit-fixture">
                <div id="owl-example" class="owl-carousel owl-theme">
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r41.jpg" alt="">
                                <p>Another Life</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r42.jpg" alt="">
                                <p>Lost In Space</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r43.jpg" alt="">
                                <p>3Below: Tales of Arcadia</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r44.jpg" alt="">
                                <p>A Series of Unfortunate Events</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r45.jpg" alt="">
                                <p>Marvel's Luke Cage</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r46.jpg" alt="">
                                <p>Marvel's The Defenders</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r47.jpg" alt="">
                                <p>Frontier</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r48.jpg" alt="">
                                <p>Chosen</p>
                            </div>
                        </a></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="row5 row">
            <h4>Crime TV Dramas</h4>
            <div id="qunit"></div>
            <div id="qunit-fixture">
                <div id="owl-example" class="owl-carousel owl-theme">
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r51.jpg" alt="">
                                <p>How to Sell Drugs Online (Fast)</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r52.jpg" alt="">
                                <p>Good Girls</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r53.jpg" alt="">
                                <p>The Sinner</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r54.jpg" alt="">
                                <p>When They See Us</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r55.jpg" alt="">
                                <p>Abyss</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r56.jpg" alt="">
                                <p>Manhunt</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r57.jpg" alt="">
                                <p>High Seas</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r58.jpg" alt="">
                                <p>Black</p>
                            </div>
                        </a></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="row6 last-div">
            <h4>Popular on Netbinge</h4>
            <div id="qunit"></div>
            <div id="qunit-fixture">
                <div id="owl-example" class="owl-carousel owl-theme">
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r61.jpg" alt="">
                                <p>Chopsticks</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r62.jpg" alt="">
                                <p>13 Reasons Why</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r63.jpg" alt="">
                                <p>Bird Box</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r64.jpg" alt="">
                                <p>Secret Obsession</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r65.jpg" alt="">
                                <p>Orange Is the New Black</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r66.jpg" alt="">
                                <p>Narcos: Mexico</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r67.jpg" alt="">
                                <p>The Perfect Date</p>
                            </div>
                        </a></div>
                    <div class="item">
                        <a href="">
                            <div class="block pull-left">
                                <img src="images/r68.jpg" alt="">
                                <p>The Kissing Booth</p>
                            </div>
                        </a></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>


        <div class="clearfix"></div>
    </div>
    <footer id="main-page-footer">
        <!--            <p>Questions? Contact Us</p>-->
        <ul>
            <li><a href="">Terms and Private Policy</a>
            </li>
            <li><a href="">FAQ</a>
                
            </li>
            <li><a href="">Help Centre</a>
                
            </li>
            <li><a href="">Send Us Feedback</a>
                
            </li>
        </ul>
        <p>Copyright &copy; 2019 <span>Netbinge</span> , All Rights Reserved.</p>
    </footer>
    <script>
    $('.owl-carousel').owlCarousel({
        //                stagePadding: 50,
        items: 4,
        //                padding: 23,
        margin: 20,
        //                autoHeight: true,
        loop: true,
        //                margin: 20,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })

</script>
</body>
</html>
