
<!DOCTYPE html>
<html>
<head>
    <title>Sign In to Netbinge</title>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="Plugins/font-awesome-4.7.0/css/font-awesome.css">
    <link rel="stylesheet" media="screen and (max-width: 768px)"
    href="css/mobile.css">
</head>
<style>
    input:invalid {
  border-style: 1px red solid;
}

input:valid {
  background-color: #ddffdd;
}

input:required {
  border-color: #800000;
  border-width: 3px;
}
    </style>

<body class="body">
    <header>
        <a href="index.php"><img src="images/netflixfinal.png" class="logo" alt="netflix-logo" width="auto" height="50px" style="margin: 17px 20px 20px 10px;"></a>
    </header>
    
<?php
	require('db.php');
	session_start();
    // If form submitted, insert values into the database.
    if (isset($_POST['email'])){
		
		$email = stripslashes($_REQUEST['email']); // removes backslashes
		$email = mysqli_real_escape_string($con,$email); //escapes special characters in a string
		$password = stripslashes($_REQUEST['password']);
		$password = mysqli_real_escape_string($con,$password);
		
	//Checking is user existing in the database or not
        $query = "SELECT * FROM `guest` WHERE email='$email' and password='$password'";
		$result = mysqli_query($con,$query) or die(mysql_error());
		$rows = mysqli_num_rows($result);
        if($rows==1){
			$_SESSION['email'] = $email;
			header("Location: home.php"); // Redirect user to index.php
            }else{
				echo "<div class='form' style='color:white;padding:20px;><h3 style='color:white;padding: 50px;font-size:30px;'>Email/password is incorrect.</h3><br/>Click here to <a href='login.php'>Sign-In</a></div>";
				}
    }else{
?>
<div class="sign-div">
        <h2>Sign In</h2>
<form action="" method="post" name="login" class="form">
<input type="text" name="email" class="email" placeholder="Email" required />
<input type="password" name="password" class="pw"  placeholder="Password"  minlength="8" required />
<a href="home.php"><button onsubmit="" class="sign-button">Sign In</button></a>
</form>
<p><a href="https://www.facebook.com" class="fb"> <i class="fa fa-facebook-official fa-2x f-icon "></i> Login with Facebook</a></p>
        <p class="new">New to Netbinge?<a href="index.php"> Sign up now</a></p>

</div>
<?php } ?>


</body>
</html>
