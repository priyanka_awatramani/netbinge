<!DOCTYPE html> 
<html> 
<head>
    <title>The Protector</title>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" media="screen and (max-width: 768px)"
    href="css/mobile.css">
</head>
<body class="video"> 
 <header class="main-page-header sticky ">
        <a href="index.php"><img src="images/netflixfinal.png" class="logo" alt="netflix-logo" width="auto" height="50px" style="margin: 17px 20px 20px 10px;"></a>
    </header>
<video width="100%" controls>
    
  <source src="The%20Protector%20-%20Official%20Trailer%20%5BHD%5D%20-%20Netflix.mp4">
</video>
<!--
<object width="425" height="350">
    <param name="movie" value="http://www.youtube.com/v/CgW_5Vthsds"></param>
    <param name="wmode" value="transparent"></param>
    <embed src="http://www.youtube.com/v/" type="application/x-shockwave-flash" wmode="transparent" width="425" height="350"></embed>
</object>
-->
<h3 class="title">The Protector</h3>
<h4 class="info">2013 &nbsp; | &nbsp;  3 Seasons</h4>
<p class="desc">When the police take time to find Keller Dover's daughter and her friend, he decides to go on a search himself. His desperation leads him closer to finding the truth and also jeopardises his own life.</p>
    <p class="add"><span class="span">Starring:</span>Hugh Jackman, Jake Jyllenhaal</p>
<p class="add"><span class="span">Creators:</span>Denis Villeneuve</p>
<footer id="footer1" class="vid">
<!--            <p>Questions? Contact Us</p>-->
            <ul>
               <li><a href="">Terms and Private Policy</a></li>
                <li><a href="">FAQ</a></li>
                <li><a href="">Help Centre</a></li>
                <li><a href="">Send Us Feedback</a></li>
            </ul>
            <p>Copyright &copy; 2019 <span>Netbinge</span> , All Rights Reserved.</p>
        </footer>

</body> 
</html>