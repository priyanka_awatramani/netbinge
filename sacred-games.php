<!DOCTYPE html> 
<html> 
<head>
    <title>Sacred Games</title>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" media="screen and (max-width: 768px)"
    href="css/mobile.css">
</head>
<body class="video"> 
 <header class="main-page-header sticky ">
        <a href="index.php"><img src="images/netflixfinal.png" class="logo" alt="netflix-logo" width="auto" height="50px" style="margin: 17px 20px 20px 10px;"></a>
    </header>
<video width="100%" controls>
    
  <source src="Sacred%20Games%202%20-%20Official%20Trailer%20-%20Netflix.mp4">
</video>
<!--
<object width="425" height="350">
    <param name="movie" value="http://www.youtube.com/v/CgW_5Vthsds"></param>
    <param name="wmode" value="transparent"></param>
    <embed src="http://www.youtube.com/v/" type="application/x-shockwave-flash" wmode="transparent" width="425" height="350"></embed>
</object>
-->
<h3 class="title">Sacred Games</h3>
<h4 class="info">2019 &nbsp; | &nbsp;  2 Seasons</h4>
<p class="desc">When police officer Sartaj Singh receives an anonymous tip about the location of criminal overlord Ganesh Gaitonde, he embarks on a chase around Mumbai in what becomes a dangerous cat-and-mouse game. Amidst the chaos, trappings of a corrupt underworld are revealed. After being removed from the Gaitonde case, Singh begins his own investigation as he works to save Mumbai from impending doom. Flashbacks reveal some of the crimes that Gaitonde has committed through the years.</p>
    <p class="add"><span class="span">Starring:</span>Saif Ali Khan, Nawazuddin Siddiqui, Pankaj Tripathi</p>
<p class="add"><span class="span">Creators:</span>Vikramaditya Motwane</p>
<footer id="footer1" class="vid">
<!--            <p>Questions? Contact Us</p>-->
            <ul>
               <li><a href="">Terms and Private Policy</a></li>
                <li><a href="">FAQ</a></li>
                <li><a href="">Help Centre</a></li>
                <li><a href="">Send Us Feedback</a></li>
            </ul>
            <p>Copyright &copy; 2019 <span>Netbinge</span> , All Rights Reserved.</p>
        </footer>

</body> 
</html>