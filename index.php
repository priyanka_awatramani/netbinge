<html>
    <head>
        <title>Netbinge</title>
        <link rel="stylesheet" href="css/styles.css">
        <link rel="stylesheet" href="Plugins/font-awesome-4.7.0/css/font-awesome.css">
        <link rel="stylesheet" media="screen and (max-width: 768px)"
    href="css/mobile.css">
    </head>
    <body class="body">
       <header>
           <img src="images/netflixfinal.png" class="logo" alt="netflix-logo" width="auto" height="50px" style="margin: 17px 20px 20px 10px;">
           <a href="login.php" class="sign-in pull-right">Sign In</a>
           <div class="clearfix"></div>
       </header>
        <div class="main-page mobmain">
            <h1 class="h1">See what's next.</h1>
            <h3 class="h3">WATCH ANYWHERE. CANCEL AT ANY TIME.</h3>
            <a href="plans.php"><button class="days-30 mob30">TRY 30 DAYS FREE</button></a> 
            <p class="pmob">Have an account? <a class="underline" href="login.php">Sign In</a></p>
        </div>
        <footer id="footer">
<!--            <p>Questions? Contact Us</p>-->
            <ul>
               <li><a href="">Terms and Private Policy</a></li>
                <li><a href="">FAQ</a></li>
                <li><a href="">Help Centre</a></li>
                <li><a href="">Send Us Feedback</a></li>
            </ul>
            <p>Copyright &copy; 2019 <span>Netbinge</span> , All Rights Reserved.</p>
        </footer>
    </body>
</html>