<!DOCTYPE html>
<html>
<head>
	<title>Plans</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="Plugins/font-awesome-4.7.0/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/plan.css">
</head>
<body>
	<header><nav class=" ">
  <a  href="index.php"><img src="images/netflixfinal.png" class="logo" alt="netflix-logo" width="auto" height="50px" style="margin: 17px 20px 20px 10px;"></a>
<!--
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
-->

   <a href="login.php" class="sign-in pull-right">Sign In</a>
               <div class="clearfix"></div>

</nav>
</header>
	<section class="main-div">
	<p class="step">STEP <strong>1</strong> OF <strong>3</strong></p>
	<h3>Choose the plan that's right for you</h3>
	<p class="down">Downgrade or upgrade at any time</p>
	<div class="coll">
	<div class="red-div" id="rdiv1">Mobile</div>
	<div class="red-div" id="rdiv2">Basic</div>
	<div class="red-div" id="rdiv3">Standard</div>
	<div class="red-div" id="rdiv4">Premium</div>
	</div>
	<table class="table">
  <thead>
    <tr>
      <th scope="col weight50">Monthly price after free month</th>
      <th class="mob"scope="col"><i class="fa fa-inr"></i> 199</th>
      <th  class="bas" scope="col"><i class="fa fa-inr"></i> 499</th>
      <th  class="st" scope="col"><i class="fa fa-inr"></i> 649</th>
      <th class="pr" scope="col"><i class="fa fa-inr"></i> 799</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row weight">HD available</th>
      <td  class="mob"><img src="images/multiply.png" width="15px" height="auto"></td>
      <td><img src="images/multiply.png" width="15px" height="auto"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
    </tr>
    <tr>
      <th scope="row weight">Ultra HD available</th>
      <td class="mob"><img src="images/multiply.png" width="15px" height="auto"></td>
      <td><img src="images/multiply.png" width="15px" height="auto"></td>
      <td><img src="images/multiply.png" width="15px" height="auto"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
    </tr>
    <tr>
      <th scope="row weight">Watch on your laptop and TV</th>
      <td class="mob"><img src="images/multiply.png" width="15px" height="auto"></td>
      <td class="bas"><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
    </tr>
    <tr>
      <th scope="row weight">Watch on your mobile phone and tablet</th>
      <td  class="mob"><img src="images/checked.png" width="auto" height="20px"></td>
      <td class="bas"><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
    </tr>
    <tr>
      <th scope="row weight">Screens you can watch on at the same time</th>
      <td class="bold">1</td>
      <td class="bold">1</td>
      <td class="bold">2</td>
      <td class="bold">4</td>
    </tr>
    <tr>
      <th scope="row weight">Unlimited films and TV programmes</th>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
    </tr>
    <tr>
      <th scope="row weight">Cancel at any time</th>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
    </tr>
    <tr>
      <th scope="row weight">First month free</th>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
      <td><img src="images/checked.png" width="auto" height="20px"></td>
    </tr>
  </tbody>
</table>
<a href="registration.php">
<button class="continue" id="cont">CONTINUE</button>
</a>
	</section>
	<footer id="footer">
<!--            <p>Questions? Contact Us</p>-->
            <ul>
               <li><a href="">Terms and Private Policy</a></li>
                <li><a href="">FAQ</a></li>
                <li><a href="">Help Centre</a></li>
                <li><a href="">Send Us Feedback</a></li>
            </ul>
            <p>Copyright &copy; 2019 <span>Netbinge</span> , All Rights Reserved.</p>
        </footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    document.getElementById("rdiv1").onclick=function(){
        document.getElementById("rdiv1").style.opacity = "1";
        document.getElementById("rdiv2").style.opacity = "0.5";
        document.getElementById("rdiv3").style.opacity = "0.5";
        document.getElementById("rdiv4").style.opacity = "0.5";
    }
    document.getElementById("rdiv2").onclick=function(){
        document.getElementById("rdiv1").style.opacity = "0.5";
        document.getElementById("rdiv2").style.opacity = "1";
        document.getElementById("rdiv3").style.opacity = "0.5";
        document.getElementById("rdiv4").style.opacity = "0.5";
    }
    document.getElementById("rdiv3").onclick=function(){
        document.getElementById("rdiv1").style.opacity = "0.5";
        document.getElementById("rdiv2").style.opacity = "0.5";
        document.getElementById("rdiv3").style.opacity = "1";
        document.getElementById("rdiv4").style.opacity = "0.5";
    }
    document.getElementById("rdiv4").onclick=function(){
        document.getElementById("rdiv1").style.opacity = "0.5";
        document.getElementById("rdiv2").style.opacity = "0.5";
        document.getElementById("rdiv3").style.opacity = "0.5";
        document.getElementById("rdiv4").style.opacity = "1";
    }
    
    
    </script>
    <script src="js/plan.js"></script>
</body>

</html>